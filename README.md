# ColourWithPerm
This plugin allows players to chat in different colors based on their permissions.

## Permissions
Chat colors can be found at <a href="http://ess.khhq.net/mc/">this website</a>.
Players may have up to two cwp permissions.
The first one is for a chat color, and the second for a modifier like bold or underline.
Two permissions aren't required. If a player has just one permission (either a color or a text modifier), that permission will show through by itself.

## Config
If you'd like to use chat prefixes/suffixes from your permissions plugin through Vault, just go into the config.yml file and change
<pre>
use-vault-prefixes: false
use-vault-suffixes: false
</pre>
to true for vault prefixes and suffixes.

### Chat Colors
<pre>
cwp.&b
cwp.&0
cwp.&9
cwp.&3
cwp.&1
cwp.&8
cwp.&2
cwp.&5
cwp.&4
cwp.&6
cwp.&7
cwp.&a
cwp.&d
cwp.&c
cwp.&f
cwp.&e
</pre>
### Text Modifiers
<pre>
cwp.&l
cwp.&n
cwp.&o
cwp.&k
cwp.&m
</pre>