package io.github.tubakyle.colourwithperm;

import net.milkbowl.vault.chat.Chat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;


public final class ColourWithPerm extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        saveDefaultConfig();
        if (getConfig().getBoolean("use-vault-prefixes") | getConfig().getBoolean("use-vault-suffixes")) {
            setupChat();
        }
    }

    public static Chat chat = null;
    private boolean setupChat()
    {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

    @EventHandler
    public void onChat (AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();
        String msg = e.getMessage();
        Character color1 = 'f';
        Character color2 = 'q';

        if (p.hasPermission("cwp.&b")) {
            color1 = 'b';
        } else if (p.hasPermission("cwp.&0")) {
            color1 = '0';
        } else if (p.hasPermission("cwp.&9")) {
            color1 = '9';
        } else if (p.hasPermission("cwp.&3")) {
            color1 = '3';
        } else if (p.hasPermission("cwp.&1")) {
            color1 = '1';
        } else if (p.hasPermission("cwp.&8")) {
            color1 = '8';
        } else if (p.hasPermission("cwp.&2")) {
            color1 = '2';
        } else if (p.hasPermission("cwp.&5")) {
            color1 = '5';
        } else if (p.hasPermission("cwp.&4")) {
            color1 = '4';
        } else if (p.hasPermission("cwp.&6")) {
            color1 = '6';
        } else if (p.hasPermission("cwp.&7")) {
            color1 = '7';
        } else if (p.hasPermission("cwp.&a")) {
            color1 = 'a';
        } else if (p.hasPermission("cwp.&d")) {
            color1 = 'd';
        } else if (p.hasPermission("cwp.&c")) {
            color1 = 'c';
        } else if (p.hasPermission("cwp.&f")) {
            color1 = 'f';
        } else if (p.hasPermission("cwp.&e")) {
            color1 = 'e';
        }

        if (p.hasPermission("cwp.&l")) {
            color2 = 'l';
        } else if (p.hasPermission("cwp.&n")) {
            color2 = 'n';
        } else if (p.hasPermission("cwp.&o")) {
            color2 = 'o';
        } else if (p.hasPermission("cwp.&k")) {
            color2 = 'k';
        } else if (p.hasPermission("cwp.&m")) {
            color2 = 'm';
        }
        String prefix = "";
        String suffix = "";
        reloadConfig();
        if (getConfig().getBoolean("use-vault-prefixes")) {
            prefix = ChatColor.translateAlternateColorCodes('&', chat.getPlayerPrefix(p) + " ");
        }
        if (getConfig().getBoolean("use-vault-suffixes")) {
            suffix = ChatColor.translateAlternateColorCodes('&', " " + chat.getPlayerSuffix(p));
        }
        e.setFormat(prefix + "" + ChatColor.RESET + "%s" + suffix + ChatColor.RESET + ": %s");
        if (color2 == 'q') {
            e.setMessage(ChatColor.getByChar('r') + "" + ChatColor.getByChar(color1) + msg);
        } else {
            e.setMessage(ChatColor.getByChar('r') + "" + ChatColor.getByChar(color1).toString() + ChatColor.getByChar(color2) + msg);
        }
    }
}